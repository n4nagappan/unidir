var Cards = Cards || {};

Cards.populateCards = function(dataItems)
{
    Cards.removeAllCards();
    $.get('templates/cardTemplate.mst', function(template) {
        
        var textToInsert = [];
        for(i in dataItems)
        {
            textToInsert.push(Mustache.render(template, dataItems[i]));
        }
        
        $('#uni-cards').append(textToInsert.join(''));
        loadMap();
    });
}

Cards.removeAllCards = function()
{
    $('#uni-cards').empty();    
}