function getLatLngInfo(searchLocation)
{
   var latInfo;
   var longInfo;
   
   var url = "https://maps.googleapis.com/maps/api/geocode/json?address=" + searchLocation + "&sensor=false&key=AIzaSyAiZBrUQbsfDdhDh0iBocmJqm2YYwHAhy0";
        
   $.ajax({
      async: false,
      dataType: "json",
      url: url,
      //data: data,
      success: function(data) {
         if(data["results"].length > 0)
         {
             latInfo = data["results"][0]["geometry"]["location"]["lat"];
             longInfo =data["results"][0]["geometry"]["location"]["lng"];
         }
      }
   });
   
        
   console.log(latInfo+", "+longInfo);
   
   return [latInfo, longInfo];
}

function loadMap() {
    var map;
    $('.uni-map').each(function() {
        
        var uniName = $(this).parent().find("div.uni-content > div.uni-content-top > div").text();
        var uniAddress = $(this).parent().find("div.uni-content > div.uni-content-bottom > div.uni-address-data").text();
        
        var latlngInfo = getLatLngInfo(uniName+" "+uniAddress);
       
       
        var latlng = new google.maps.LatLng(latlngInfo[0], latlngInfo[1]);
       //console.log(latlngInfo);
       var myOptions = {
           zoom: 15,
           zoomControl: false,
           streetViewControl: false,
           mapTypeControl: false,
           center: latlng,
           mapTypeId: google.maps.MapTypeId.ROADMAP
       };
       var map = new google.maps.Map(this,
               myOptions);
       var marker = new google.maps.Marker({
          position: latlng, 
          Symbol: {
               path: google.maps.SymbolPath.CIRCLE,
               scale: 3
               },
          map: map});
      
    });
}

function initialize() {

  console.log("initialize function called");

    $("#searchButton").click(function(event){
        var searchText = $("#searchText").val();
        WebServiceProxy.fetchData(Cards.populateCards,searchText);
    });
    
    //Triggers search on enter
    $("#searchText").keyup(function(event){
        if(event.keyCode == 13){
            $("#searchButton").click();
        }
    });
}

$(document).ready(function () {
    console.log("Card script loaded");
  
    // calling the 0th page of the results
    WebServiceProxy.fetchData(Cards.populateCards);
    initialize();
});

