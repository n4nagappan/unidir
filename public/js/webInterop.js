var WebServiceProxy = WebServiceProxy || {};


WebServiceProxy.fetchData = function(successCallback, searchText)
{
    var query = "";

    if(searchText && searchText != "")
        query = "&text="+searchText;
    
    var url = "http://colscrap.cloudapp.net/v1/colleges?page=0" + query + '&callback=?';
    $.getJSON(url,successCallback);
}
